import { expect } from 'chai';
import { isAnagram } from './anagram';

describe('isAnagram - basic functionality', () => {
    it('returns true if listen and silent are an anagram', () => {
        const actual = isAnagram('listen', 'silent');
        expect(actual).to.be.true;
    });
    it('returns true if below and elbow are an anagram', () => {
        const actual = isAnagram('below', 'elbow');
        expect(actual).to.be.true;
    });
    it('returns false if listens and silent is not an anagram', () => {
        const actual = isAnagram('listens', 'silent');
        expect(actual).to.be.false;
    });
    it('returns false if belows and elbow is not an anagram', () => {
        const actual = isAnagram('belows', 'elbow');
        expect(actual).to.be.false;
    });
    it('returns true if the two strings are an anagram ignoring spaces in the strings', () => {
        const actual = isAnagram('conversation', 'voices rant on');
        expect(actual).to.be.true;
    });
    it('returns true if the two strings are an anagram ignoring case in the strings', () => {
        const actual = isAnagram('STATE', 'taste');
        expect(actual).to.be.true;
    });
});
