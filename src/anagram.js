import { isEqual } from 'lodash';
import { getLetterCount } from './letter-count';

export const isAnagram = (stringOne, stringTwo) => {
    /* Checking if two strings are an anagram */
    // const modifiedStringOne = stringOne.split('').sort().join('').toLowerCase().trim();
    // const modifiedStringTwo = stringTwo.split('').sort().join('').toLowerCase().trim();
    // if (modifiedStringOne === modifiedStringTwo) {
    //     return true;
    // } else {
    //     return false;
    // }

    /* Checking if two strings are an anagram using lodash */
    const modifiedStringOne = getLetterCount(stringOne.toLowerCase().replace(/\s/g, ''));
    const modifiedStringTwo = getLetterCount(stringTwo.toLowerCase().replace(/\s/g, ''));
    return isEqual(modifiedStringOne, modifiedStringTwo);
};
